from django.contrib.auth.models import User
from django.db.utils import IntegrityError

try:

    user = User.objects.create_user('admin', password='admin')
    user.is_superuser = True
    user.is_staff = True
    user.save()

except IntegrityError:
    print("The super user already exists. Creation failed")
