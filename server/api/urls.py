from django.conf.urls import url, include
from rest_framework import routers
from .views import GateWayViewSet, MeasureViewSet, SensorViewSet

router = routers.DefaultRouter()
router.register(r'gateways', GateWayViewSet)
router.register(r'measures', MeasureViewSet)
router.register(r'sensors', SensorViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    url(r'^', include(router.urls))
    #url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
