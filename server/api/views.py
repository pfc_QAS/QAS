from rest_framework.decorators import action
from rest_framework.response import Response
from web.models import Gateway, Sensor, Measure
from rest_framework import viewsets
from .serializers import GatewaySerializer, SensorSerializer, MeasureSerializer


class GateWayViewSet(viewsets.ModelViewSet):
    queryset = Gateway.objects.all().order_by('-addDate')
    serializer_class = GatewaySerializer


class SensorViewSet(viewsets.ModelViewSet):
    queryset = Sensor.objects.all()
    serializer_class = SensorSerializer

    @action(methods=['get'], detail=True, url_name='last-measure', url_path='last-measure')
    def last_measure(self, request, pk):
        last_sensor_measure = Measure.objects \
            .all() \
            .filter(sensor_id=pk) \
            .order_by('-mes_date') \
            .first()
        serializer_class = MeasureSerializer(last_sensor_measure, many=False, context={'request': request})
        return Response(serializer_class.data)

    @action(methods=['get'], detail=True, url_name='measures', url_path='measures')
    def measures(self, request, pk):
        date_from = self.request.query_params.get('date_from')
        date_to = self.request.query_params.get('date_to')
        measures = Measure.objects \
            .all() \
            .filter(sensor_id=pk, mes_date__gte=date_from, mes_date__lte=date_to).order_by('-mes_date')
        serializer_class = MeasureSerializer(measures, many=True, context={'request': request})
        return Response(serializer_class.data)


class MeasureViewSet(viewsets.ModelViewSet):
    queryset = Measure.objects.all()
    serializer_class = MeasureSerializer
