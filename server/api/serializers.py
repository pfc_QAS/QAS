from web.models import Gateway, Sensor, Measure
from rest_framework import serializers


class GatewaySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Gateway
        fields = ('id', 'public', 'name', 'description', 'addDate', 'token', 'configuration')


class SensorSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Sensor
        fields = ('id', 'name', 'description', 'addDate', 'public', 'type', 'configuration', 'gateway')


class MeasureSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Measure
        fields = ('id', 'value', 'valueInt', 'unit', 'type', 'mes_date', 'sensor')
