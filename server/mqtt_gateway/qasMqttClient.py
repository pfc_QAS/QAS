#!/usr/bin/python3
# Copyright (C) 2018 Brais Arias Rio
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import paho.mqtt.client as mqtt
import os, sys
import django
import time
os.environ["DJANGO_SETTINGS_MODULE"] = 'framework.settings'
django.setup()
from web.models import Gateway, Sensor, Measure
# constants
#TODO think what i want to do to handle it
# TODO, we need to extract this out of repo for security reasons.
qasTopic = "qas/measures"
mqttServer = "qas-mqtt.xeitodevs.com"
mqttPort = 443
mqttUser = "qasuser"
mqttPassword = "nHZFtS0S5gxe0ZW"


def on_connect(client, userdata, flags, rc):
    client.subscribe("qas/measures")
    # client.subscribe(qasTopic)
    print("Connected to mqtt")

def on_message(client, userdata, msg):
    import traceback
    def saveMeasureFromJSON(jsonData):
        from web.models import Sensor, Measure
        import json
        try:
            dicM = json.loads(jsonData)
            newM = Measure()
            newM.value = dicM['value']
            newM.valueInt = float(dicM['value'])
            newM.unit = dicM['unit']
            newM.type = dicM['type']
            newM.unit = dicM['unit']

            sIdStr = dicM['device']
            sId = int(sIdStr[sIdStr.find('S')+1:])
            s = Sensor.objects.get(pk=sId)
            newM.sensor = s
            timeEpoch = int(dicM['time'])
            if timeEpoch:
                #TODO convert and assign
                pass
            newM.save()
        except Exception as err:
            print(err)
            traceback.print_tb(err.__traceback__)
    import time
    nowStr = str(time.strftime('%Y-%m-%d_%H:%M:%S'))
    try:
        msgstr = str(msg.payload.decode("utf-8"))
        print("[", nowStr, "] ", str(msg.topic), " >> " , msgstr)
        saveMeasureFromJSON(msgstr)
    except Exception as err:
        print("[", nowStr, "] ", str(msg.topic), " > ERROR with message: " , str(msg))
        print(err)
        traceback.print_tb(err.__traceback__)


client = mqtt.Client()
client.tls_set()
client.on_connect = on_connect
client.on_message = on_message
client.connect(mqttServer, mqttPort, 60)
client.loop_forever()
