# Copyright (C) 2018 Brais Arias Rio
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse
from django.template import loader
from .models import Gateway, Sensor, Measure


# Create your views here.
def index(request):
    template = loader.get_template('index.html')
    return render(request, 'index.html', {})
    # return HttpResponse("PFC QAS, sistema de sensorización da calidade do aire")


def listGateways(request):
    listGtw = Gateway.objects.order_by('-name')

    return render(request, 'listGateways.html', {'listGateways': listGtw})


# like a gateway details and lista of sensors of this gateway
def listSensors(request, gtw_id):
    gateway = get_object_or_404(Gateway, id=gtw_id)
    sensors = gateway.sensors.all().order_by('-name')
    # sensors = Sensor.objects.order_by('-name')

    return render(request, 'gatewayDetail.html', {'sensors': sensors, 'gateway': gateway})


def listMeasures(request, sensor_id):
    sensor = get_object_or_404(Sensor, id=sensor_id)
    measures = sensor.measures.all().order_by('-mes_date');
    # measures = Measure.objects.filter(sensor).order_by('-mes_date')

    return render(request, 'measures.html', {'sensor': sensor, 'measures': measures})


def dashboard(request):
    return render(request, 'dashboard.html')
