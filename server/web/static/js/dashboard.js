/* eslint-disable no-undef */

class SensorsRepository {
  async getAllSensors () {
    const response = await axios.get('/api/v1/sensors')
    return response.data
  }
}

class MeasureRepository {
  async getLastMeasureFromSensor (sensorId) {
    const response = await axios.get(`/api/v1/sensors/${sensorId}/last-measure`)
    return response.data
  }

  async getAllMeasuresPerSensorWithInterval (sensorId, dateFrom, dateTo) {
    const response = await axios.get(`/api/v1/sensors/${sensorId}/measures`, {
      params: {
        date_from: dateFrom,
        date_to: dateTo
      }
    })
    return response.data
  }
}

function createHTMLChart (sensor) {

  const container = document.createElement('div')
  container.className = 'four columns'

  const canvas = document.createElement('canvas')
  canvas.setAttribute('id', `s_${sensor.id}`)
  canvas.setAttribute('width', '400')
  canvas.setAttribute('height', '400')

  container.appendChild(canvas)
  document.querySelector('#sensor-graphs').appendChild(container)
  return canvas
}

function randomRgba () {

  const availableColours = [
    'rgba(49, 4, 180, 1)',
    'rgba(8, 138, 8, 1)',
    'rgba(255, 0, 128, 1)',
    'rgba(172, 88, 250, 1)'
  ]
  return availableColours[Math.floor(Math.random()*availableColours.length)]
}

function createChartForSensor (sensor) {
  const htmlChart = createHTMLChart(sensor)
  const measuresUnit = sensor.measures[0].unit
  const measuresData = sensor.measures.map((measure) => {
    return {
      x: measure.mes_date,
      y: measure.valueInt
    }
  })

  return new Chart(htmlChart, {
    type: 'line',
    data: {
      datasets: [{
        label: `Measures for ${sensor.name}`,
        data: measuresData,
        backgroundColor: 'rgba(0,0,0,0)',
        borderColor: [
          randomRgba()
        ],
        borderWidth: 1
      }]
    },
    options: {
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          },
          scaleLabel: {
            display: true,
            labelString: `Units (${measuresUnit})`
          }
        }],
        xAxes: [{
          type: 'time',
          time: {
            unit: 'minute'
          }
        }]
      }
    }
  })
}

function createLastMeasureForSensor (sensor) {
  const container = document.createElement('div')
  container.className = 'last-measure'
  container.innerHTML = `${sensor.name}<br>${sensor.measures[0].valueInt} ${sensor.measures[0].unit}<br>`
  document.querySelector('#sensors-last-measures').appendChild(container)

}

(async function () {
  const timeout = 10000
  const getMeasuresFromDaysAgo = 1
  const sensorsRepository = new SensorsRepository()
  const measureRepository = new MeasureRepository()
  const dateFrom = moment().subtract(getMeasuresFromDaysAgo, 'day').utc()
  const dateTo = moment().utc()
  const sensors = await sensorsRepository.getAllSensors()
  for (const sensor of sensors) {
    sensor.measures = await measureRepository.getAllMeasuresPerSensorWithInterval(
      sensor.id, dateFrom.toISOString(), dateTo.toISOString()
    )
    if (sensor.measures.length > 0) {
      createLastMeasureForSensor(sensor)
      createChartForSensor(sensor)
    }
  }
  setTimeout(() => {
    window.location.reload()
  }, timeout)
})()
