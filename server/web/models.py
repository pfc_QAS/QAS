# Copyright (C) 2018 Brais Arias Rio
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

from django.db import models
from django.conf import settings

class Gateway(models.Model):
    #id = models.CharField(max_length=200)
    # privacity public or private
    public = models.BooleanField(default=True)
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=200, null=True, blank=True)
    addDate = models.DateTimeField()
    token = models.CharField(max_length=200)
    # TODO this is a provisional security
    configuration = models.TextField(null=True, blank=True)
    # TODO User, but we need to determine the way to implement
    # it in order to authentication
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    def __str__(self):
        return ' '.join((self.name, 'de', str(self.owner)))

class Sensor(models.Model):
    #id = models.CharField(max_length=200)
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=200, null=True, blank=True)
    addDate = models.DateTimeField()
    public = models.BooleanField(default=True, blank=True)
    # privacity public or private
    type = models.CharField(max_length=200, null=True, blank=True)
    configuration = models.TextField(null=True, blank=True)
    gateway = models.ForeignKey('Gateway', on_delete=models.CASCADE, related_name='sensors')
    # TODO geoposition point...

    def __str__(self):
        return ' '.join((self.name, 'en' , str(self.gateway)))

class Measure(models.Model):
    value = models.CharField(max_length=200)
    valueInt = models.FloatField(default=0)
    unit = models.CharField(max_length=200)
    type = models.CharField(max_length=200)
    mes_date = models.DateTimeField('date and time of the measure', auto_now=True)
    sensor = models.ForeignKey('Sensor', on_delete=models.CASCADE, related_name='measures')

    def __str__(self):
        return ' '.join((self.value, self.unit))
