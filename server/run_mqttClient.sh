#!/bin/bash

# Copyright (C) 2018 Brais Arias Rio
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

# Load virtual environment
source  ~/virtualenvironment/pfcqas/bin/activate

# Ensure you are in the correct directory
cd ~/pfc/git/QAS/server/


# Run the MQTT client/API/Bridge
python manage.py  shell < mqtt_gateway/qasMqttClient.py
