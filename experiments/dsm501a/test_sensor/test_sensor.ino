int pm10 = 6;
int pm25 = 3;

long conc_pm10 = 0;
long conc_pm25 = 0;

void setup() {
  Serial.begin(115200);
  pinMode(pm10, INPUT);
  pinMode(pm25, INPUT);
  
}

long getPM(int pinSensor) {
    uint32_t lowpulseoccupancy=0;
    uint32_t starttime=0;
    uint32_t duration=0;
    uint32_t endtime=0;
    uint32_t sampletime_ms = 30000;
    float ratio;

    starttime = millis();
    while (1) {
        duration = pulseIn(pinSensor, LOW);
        lowpulseoccupancy += duration;
        endtime = millis();

        if ((endtime-starttime) >= sampletime_ms) {
        ratio = lowpulseoccupancy / (sampletime_ms*10.0);
        //ratio = (lowpulseoccupancy-endtime+starttime)/(sampletime_ms*10.0);  // Integer percentage 0=>100 -> Not working
        long concentration = 1.1 * pow( ratio, 3) - 3.8 *pow(ratio, 2) + 520 * ratio + 0.62; // using spec sheet curve
        //if (_debugMode){
            Serial.print("\nratio: ");
            Serial.print(ratio);
        //}
        lowpulseoccupancy = 0;
        return(concentration);
        }
    }
}



void loop() {
  delay(500);
  Serial.println("Req pm10");
  conc_pm10 = getPM(pm10);
  Serial.print("\n");
  Serial.print("PM 10: ");
  Serial.print(conc_pm10);
  Serial.print("\n");
  delay(500);
  Serial.println("Req pm2.5");
  conc_pm25 = getPM(pm25);
  Serial.print("\n");
  Serial.print("PM 2.5: ");
  Serial.print(conc_pm25);
  Serial.print("\n");
}
