#include<Wire.h>

const int slaveId = 2;
const int lenMsg = 100;

void setup() {
  Wire.begin(1);//#1 is our master
  Serial.begin(9600);
  Serial.print("Initialized master\n");
}

void loop() {
  Serial.print("request:\n");
  Wire.requestFrom(slaveId, lenMsg);

  Serial.print("wait\n");
  while (Wire.available()) {
    char c = Wire.read();
    Serial.print(c);
  }

  delay(500);
}
