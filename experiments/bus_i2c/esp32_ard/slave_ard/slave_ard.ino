#include<Wire.h>

const int slaveId = 2;
const int lenMsg = 100;

const float mookVal = 999.999;
const char mookUnit[] = "mg/cm3";

void setup() {
  Wire.begin(slaveId);
  Serial.begin(9600);
  Wire.onRequest(requestEvent);
  Serial.print("Initialized slave\n");
}

void loop() {
  delay(100);
}

void requestEvent() {
  Serial.print("RequestEvent\n");
  String msg = "value:" + String(mookVal) + ":unit:" + mookUnit + "\0";
  char msgB[lenMsg];
  Serial.print(msg);
  msg.getBytes(msgB, lenMsg);
  Wire.write(msgB);
}
