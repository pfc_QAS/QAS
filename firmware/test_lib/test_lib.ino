// Copyright (C) 2018 Brais Arias Rio
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>

#define ARCH esp32

#include <QAS.h>

/*
 *  Only for Arduino pro mini
 */
String lastMeasure;
String unit = "ug/m3";
int lenMsg = 100;

//void requestEvent() {
//  Serial.print("RequestEvent\n");
//  unsigned int s = lastMeasure.indexOf(":");
//  String msg = lastMeasure.substring(0, s) + ":" + unit + "\0";
//  char msgB[lenMsg];
//  Serial.print(msg);
//  msg.getBytes(msgB, lenMsg);
//  Wire.write(msgB);
//}
/* ********** */


  //char * ssid = "Gondor2";
  //char * password = "un,dos,tres;zapatito_ingles!!!";
  //char * mqttServer = "192.168.101.66";

//  char * ssid = "QAS_demo";
//  char * password = "BraisAriasRio";
//  char * mqttServer = "10.42.0.1";

    char * ssid = "Argalleiros lab";
  char * password = "BraisAriasRio";
  char * mqttServer = "192.168.1.13";

  int port = 1883;


/* this is the objects for esp32 gateway
 * with sensors connected with arduino
 * adapter with i2c */
//QAS esp32_prot(i2cibus, wifi, gtw, true);
//QAS_dev_dsm501a ard1_prot(direct, i2cebus, true, 6, 3);
//QAS_dev_sharpgp2y10 ard2_prot(direct, i2cebus, true, 0);


/*
 * This is the case of dsm501a sensor connected directly to gateway
 */
//ESP con dsm501
QAS_dev_dsm501a esp_dsm(direct, wifi, true, 4, 2);
// pin pm10   4   vermello
// pin pm2.5  2   rosa

// Sharp en Arduino (adaptador)
//QAS_dev_sharpgp2y10 ard_gp2(direct, i2cebus, true, 0, 2);
// pin do sensor A0 negro
// pin do led D2    verde

// Sharp en ESP32
QAS_dev_sharpgp2y10 esp_gp2(i2cibus, wifi, true, 0, 0);
//

void setup() {
  Serial.begin(115200);

  esp_dsm.setSsid(ssid);
  esp_dsm.setPassword(password);
  esp_dsm.setMqttServer(mqttServer);

  esp_dsm.setMqttPort(port);
  esp_dsm.setDevice("GTW2S2");
  esp_dsm.setup();

  /*
   *  Only for Arduino pro mini
   */
//  ard_gp2.setup();
//  Wire.begin(2);
//  Wire.onRequest(requestEvent);
  /* ************ */

  esp_gp2.setSsid(ssid);
  esp_gp2.setPassword(password);
  esp_gp2.setMqttServer(mqttServer);
  esp_gp2.setMqttPort(port);

  esp_gp2.setDevice("GTW2S3");
  esp_gp2.setup();
}

void loop() {
  esp_dsm.loop();
  delay(30000);

  esp_gp2.loop();

  /*
   * only for arduino
   */
//  ard_gp2.loop();
//  lastMeasure = ard_gp2.getMeasure();
//  delay(30000);
  /* *************** */

  delay(30000);
  //delay(120000);
}
