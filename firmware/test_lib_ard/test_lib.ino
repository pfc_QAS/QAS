#include <QAS.h>

String lastMeasure;
String unit = "ug/m3";
int lenMsg = 100;

void requestEvent() {
  Serial.print("RequestEvent\n");
  unsigned int s = lastMeasure.indexOf(":");
  String msg = lastMeasure.substring(0, s) + ":" + unit + "\0";
  char msgB[lenMsg];
  Serial.print(msg);
  msg.getBytes(msgB, lenMsg);
  Wire.write(msgB);
}
/* this is the objects for esp32 gateway
 * with sensors connected with arduino
 * adapter with i2c */
//QAS esp32_prot(i2cibus, wifi, gtw, true);
//QAS_dev_dsm501a ard1_prot(direct, i2cebus, true, 6, 3);
//QAS_dev_sharpgp2y10 ard2_prot(direct, i2cebus, true, 0);


/*
 * This is the case of dsm501a sensor connected directly to gateway
 */
//ESP con dsm501
//QAS_dev_dsm501a esp_dsm(direct, wifi, true, 4, 2);
// pin pm10   4   vermello
// pin pm2.5  2   rosa

// Sharp en Arduino (adaptador)
QAS_dev_sharpgp2y10 ard_gp2(direct, i2cebus, true, 0, 2);
// pin do sensor A0 negro
// pin do led D2    verde

// Sharp en ESP32
//QAS_dev_sharpgp2y10 esp_gp2(i2cibus, wifi, true, 0, 2);
//

void setup() {
  Serial.begin(115200);

  //esp_dsm.setSsid("Gondor2");
  //esp_dsm.setPassword("un,dos,tres;zapatito_ingles!!!");
  //esp_dsm.setMqttServer("192.168.101.66");
  
  //esp_dsm.setSsid("QAS_demo");
  //esp_dsm.setPassword("BraisAriasRio");
  //esp_dsm.setMqttServer("10.42.0.1");
  
  //esp_dsm.setMqttPort(1883);
  //esp_dsm.setDevice("GTW2S2");
  //esp_dsm.setup();

  ard_gp2.setup();
  Wire.begin(2);
  Wire.onRequest(requestEvent);
}

void loop() {
  //esp_dsm.loop();

  ard_gp2.loop();
  lastMeasure = ard_gp2.getMeasure();
  
  delay(30000);
}
