#ifndef QAS_h
#define QAS_h

#include <Wire.h>
#include <Arduino.h>
#include <Wire.h>
#include <ArduinoJson.h>
//#include <WiFi.h>
#include <PubSubClient.h>


/* *************************************************
 * DATATYPES
 * *************************************************/

/* This is the datatype which define the kind of internal bus you have in this device */
enum QASiBusT {
    direct,
    i2cibus
};

/* This is the datatype which define the kind of external bus you have in this device */
enum QASeBusT {
	i2cebus,
    wifi
};

enum QASsensorT {
    gtw,
    dsm501a,
    sharpgp2y10
};


/* *************************************************
 * CLASSES
 * *************************************************/


/* *************************************************
 * This is the generic class
 * *************************************************/
class QAS
{
    public:
        QAS(QASiBusT iBus,
            QASeBusT eBus,
            QASsensorT sensor,
            bool debugMode
           );
        /*
         * This function read measure from internal bus
         */
        String getMeasure();
        //void sendRemote(char * msg);
        void sendRemote(String msg);
        void setup();
        void loop();
        void setSsid(char * ssid);
        void setPassword(char * password);
        void setDevice(char * device);
        void setMqttServer(char * server);
        void setMqttPort(int port);
    protected:
        QASiBusT _iBus;
        QASeBusT _eBus;
        QASsensorT _sensor;
        unsigned long _periodicity = 300 * 1000; // in miliseconds
        bool _debugMode;
        String _lastMeasure;

        void sendWifi(char * msg);

        char* _ssid;
        char* _password;

        char* _mqttServer;
        int _mqttPort = 1883;
        char* _mqttUser;
        char* _mqttPassword;
        void connectWiFi(int attempts);
        void connectMQTT(int attempts);
        bool _wifiConnected;
        bool _mqttConnected;
        char* _device;

};


/* *************************************************
 * This is the generic class for sensor adapters
 * *************************************************/
class QAS_Adapter : public QAS
{
public:
    QAS_Adapter(QASiBusT iBus,
                QASeBusT eBus,
                QASsensorT sensor,
                bool debugMode,
                int sensorPin
               );
    void setPinout();
    String getMeasure();
    void setup();
protected:
	int _sensorPin;
  int _i2cId = 2;
  int _lenMsg = 100;
  String _unit = "ug/m3";
};

/* *************************************************
 * This is the class for dsm501a sensor
 * *************************************************/
class QAS_dev_dsm501a : public QAS_Adapter
{
public:
    QAS_dev_dsm501a(QASiBusT iBus,
                    QASeBusT eBus,
                    bool debugMode,
                    int s_pm10,
                    int s_pm25
                );
    String getMeasure();
    void setup();
    void loop();
    String _unit = "ug/m3";
    String _type_pm10 = "PM10";
    String _type_pm25 = "PM2.5";
    //TODO configure that with device MAC
    String _device = "GTW2S2";
    char * _topic = "qas/measures";
private:
    bool _wifiConnected;
    bool _mqttConnected;
    int _s_pm10;
    int _s_pm25;
    long getPM(int pinSensor);
    String buildJSON(
      String device,
      String type,
      String value,
      String unit,
      String timestamp
    );
};


/* *************************************************
 * This is the class for sharpgp2y10 sensor
 * *************************************************/
class QAS_dev_sharpgp2y10 : public QAS_Adapter
{
public:
    QAS_dev_sharpgp2y10(QASiBusT iBus,
                        QASeBusT eBus,
                        bool debugMode,
                        int sensorPin,
                        int ledPin
                       );
    String getMeasure();
    void setup();
    void loop();
  private:
    int _ledPin;
    int _samplingTime = 280;
    int _deltaTime = 40;
    int _sleepTime = 9680;
    String getDirectMeasure();
    void getMeasureOnRequest();
};


#endif
