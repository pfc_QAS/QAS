#include "QAS.h"

//WiFiClient _espClient;
//PubSubClient client(_espClient);

/*
 * QAS class
 */
QAS::QAS(QASiBusT iBus,
         QASeBusT eBus,
         QASsensorT sensor,
         bool debugMode
        )
{
    _iBus = iBus;
    _eBus = eBus;
    _sensor = sensor;
    _debugMode = debugMode;
    _wifiConnected = false;
    _mqttConnected = false;

    _mqttUser = new char[5];
    strcpy(_mqttUser, "xx\0");
    _mqttPassword = new char[5];
    strcpy(_mqttPassword, "xx\0");
}

void QAS::setSsid(char * ssid)
{
  delete [] _ssid;
  _ssid = new char[strlen(ssid+1)];
  strcpy(_ssid, ssid);
}

void QAS::setPassword(char * password)
{
  delete [] _password;
  _password = new char[strlen(password+1)];
  strcpy(_password, password);
}

void QAS::setDevice(char * device)
{
  delete [] _device;
  _device = new char[strlen(device+1)];
  strcpy(_device, device);
}

void QAS::setMqttServer(char * server)
{
  delete [] _mqttServer;
  _mqttServer = new char[strlen(server+1)];
  strcpy(_mqttServer, server);
}

void QAS::setMqttPort(int port)
{
  _mqttPort = port;
}

String QAS::getMeasure()
{
    if (_debugMode){
        Serial.print("Reading measure");
    }
    return "";
}


// void QAS::connectWiFi(int attempts){
//   int count=0;
//   WiFi.begin(_ssid, _password);
//   while ((WiFi.status() != WL_CONNECTED) && count < attempts) {
//     delay(5000);
//     Serial.println("Connecting to WiFi..");
//     count++;
//   }
//   if (WiFi.status() != WL_CONNECTED){
//     _wifiConnected = true;
//     Serial.println("Connected to the WiFi network");
//     Serial.print("IP address: ");
//     Serial.println(WiFi.localIP());
//     Serial.print("MAC address: ");
//     Serial.println(WiFi.macAddress());
//   } else{
//     _wifiConnected = false;
//     Serial.println("NOT Connected to WiFi");
//   }
// }
//
// void QAS::connectMQTT(int attempts){
//   int count=0;
//   client.setServer(_mqttServer, _mqttPort);
//
//   while ((!client.connected()) && count < attempts) {
//     Serial.println("Connecting to MQTT...");
//     if (client.connect(_device, _mqttUser, _mqttPassword )) {
//       _mqttConnected = true;
//       Serial.println("MQTT connected");
//     } else {
//       _mqttConnected = false;
//       Serial.print("MQTT failed with state ");
//       Serial.println(client.state());
//       delay(2000);
//     }
//     count++;
//   }
// }

void QAS::setup()
{
    Serial.println("Configuring");
    if(_eBus == wifi){
        // connectWiFi(2);
        // connectMQTT(5);
      if (_debugMode){
        Serial.println("Configuration:");
        Serial.print("\n _mqttServer: ");
        Serial.print(_mqttServer);
        Serial.print("\n _mqttUser: ");
        Serial.print(_mqttUser);
        Serial.print("\n _mqttPassword: ");
        Serial.print(_mqttPassword);
        Serial.print("\n _device: ");
        Serial.print(_device);
        Serial.print("\n _ssid: ");
        Serial.print(_ssid);
        Serial.print("\n _password: ");
        Serial.print(_password);
        Serial.println("\n---------------");
      }
    }
}


// void QAS::sendWifi(char *msg)
// {
//     // check MQTT connectivity
//     // if not connected, reconnect
//     // if fail -> print log
//     if (client.publish("qas/measures", msg) == true) {
//         Serial.println("Success sending message");
//     } else {
//         Serial.println("Error sending message");
//     }
//     client.loop();
// }


void QAS::sendRemote(String msg)
{
    if (_debugMode){
        Serial.print("Send measure to remote");
    }
    switch(_eBus){
        case wifi:
            break;
        case i2cebus:
            _lastMeasure = msg;
            break;
        default:
        Serial.print("unsuported external bus");
    }
}


/*
 * QAS_Adapter subclass
 */


QAS_Adapter::QAS_Adapter(QASiBusT iBus,
                         QASeBusT eBus,
                         QASsensorT sensor,
                         bool debugMode,
                         int sensorPin
                        ) : QAS(iBus, eBus, sensor, debugMode)
{
    _sensorPin = sensorPin;
}

void QAS_Adapter::setup()
{
  QAS::setup();
}

void QAS_Adapter::setPinout()
{
    Serial.print("set pinout");
}



QAS_dev_dsm501a::QAS_dev_dsm501a(QASiBusT iBus,
                                QASeBusT eBus,
                                 bool debugMode,
                                 int s_pm10,
                                 int s_pm25
                                ) : QAS_Adapter(iBus, eBus, dsm501a, s_pm10, debugMode)
{
    _s_pm10 = s_pm10;
    _s_pm25 = s_pm25;
}

void QAS_dev_dsm501a::setup()
{
  QAS_Adapter::setup();
  pinMode(_s_pm10, INPUT);
  pinMode(_s_pm25, INPUT);
}

long QAS_dev_dsm501a::getPM(int pinSensor) {
    uint32_t lowpulseoccupancy=0;
    uint32_t starttime=0;
    uint32_t duration=0;
    uint32_t endtime=0;
    uint32_t sampletime_ms = 30000;
    float ratio;

    starttime = millis();
    while (1) {
        duration = pulseIn(pinSensor, LOW);
        lowpulseoccupancy += duration;
        endtime = millis();

        if ((endtime-starttime) >= sampletime_ms) {
          //ratio = lowpulseoccupancy / (sampletime_ms*10.0);
          ratio = (lowpulseoccupancy-endtime+starttime)/(sampletime_ms*10.0);  // Integer percentage 0=>100 -> Not working
          //long concentration = 1.1 * pow(ratio, 3) - 3.8 * pow(ratio, 2) + 520 * ratio + 0.62; // using spec sheet curve
          long concentration = 0.1776 * pow(ratio, 3) - 2.24 * pow(ratio, 2) + 94.003 * ratio; // using spec sheet curve
          if (_debugMode){
            Serial.print("\n---DSM501A------");
            Serial.print("lowpulseoccupancy:");
        		Serial.print(lowpulseoccupancy);
        		Serial.print("\n");
        		Serial.print("ratio:");
        		Serial.print(ratio);
        		Serial.print("\n");
        		Serial.print("concentration:");
        		Serial.println(concentration);
            Serial.print("\n--------------");
          }
          lowpulseoccupancy = 0;
          return(concentration);
        }
    }
}

String QAS_dev_dsm501a::getMeasure()
{
    if (_debugMode){
        Serial.print("Reading measure from dsm501a");
    }

    long pm10 = getPM(_s_pm10);
    if (_debugMode){
        Serial.print("\nPM 10: ");
        Serial.print(pm10);
    }
    long pm25 = getPM(_s_pm25);
    if (_debugMode){
        Serial.print("\nPM 2.5: ");
        Serial.print(pm25);
    }

    String ret = String(pm10) + ":" + String(pm25);
    if (_debugMode){
      Serial.print("\nMeasure: ");
      Serial.println(ret);
    }

    return ret;
}

String QAS_dev_dsm501a::buildJSON(
  String device,
  String type,
  String value,
  String unit,
  String timestamp
)
{
  StaticJsonBuffer<300> JSONbuffer;
  JsonObject& JSONencoder = JSONbuffer.createObject();

  JSONencoder["device"] = device;
  JSONencoder["type"] = type;
  JSONencoder["value"] = value;
  JSONencoder["unit"] = unit;
  JSONencoder["time"] = timestamp;

  char JSONmessageBuffer[100];

  JSONencoder.printTo(JSONmessageBuffer, sizeof(JSONmessageBuffer));

  return String(JSONmessageBuffer);
}


void QAS_dev_dsm501a::loop()
{
  String measure = getMeasure();
  if (_debugMode){
    Serial.print("\nOn loop: ");
    Serial.println(measure);
  }
  // client.loop();//TODO sendExternal
  // measure = <pm10>:<pm2.5>

  unsigned int s = measure.indexOf(":");
  String msg_pm10 = buildJSON(_device, _type_pm10, measure.substring(0, s) , _unit, "000000");
  String msg_pm25 = buildJSON(_device, _type_pm25, measure.substring(s+1, measure.length()) , _unit, "000000");
  if (_debugMode){
    Serial.println(msg_pm10);
    Serial.println(msg_pm25);
  }
  // client.loop();

  //TODO mandar medicion a remoto
  //TODO este envio ten que comprobar conexión WiFI e MQTT
  char msg[100];
  msg_pm10.toCharArray(msg, 100);
  // connectWiFi(1);
  // connectMQTT(1);
  // if (client.publish(_topic, msg) == true) {
  //   Serial.println("Success sending PM10");
  // } else {
  //   Serial.println("Error sending PM10");
  // }
  msg_pm25.toCharArray(msg, 100);
  // if (client.publish(_topic, msg) == true) {
  //   Serial.println("Success sending PM2.5");
  // } else {
  //   Serial.println("Error sending PM2.5");
  // }

  //TODO en lugar de todo iso:
  // if (!sendRemote(msg)) {
  //   saveLocal(msg);
  //   pendingMsg++;
  // }
  //sendRemote(msg): ten que
  // revisar se hai mensaxes pendentes:
  // se hai enviamos primeiro pendentes : sumamos / restamos en función dos que quedan
  // se non demos enviado temos que gardar en local e sumar pendentes

  //o getMeasure ten que ter mirar se a medida ven directo ou ven de i2c

  // client.loop();

  //TODO gardar medicion en local
  delay(_periodicity/2);

}


QAS_dev_sharpgp2y10::QAS_dev_sharpgp2y10(QASiBusT iBus,
                                         QASeBusT eBus,
                                         bool debugMode,
                                         int sensorPin,
                                         int ledPin
                                ) : QAS_Adapter(iBus, eBus, sharpgp2y10, sensorPin, debugMode)
{
  _ledPin = ledPin;
  //TODO ic2pin que pasa con isto
}

// void QAS_dev_sharpgp2y10::getMeasureOnRequest()
// {
//   Serial.print("RequestEvent\n");
//   unsigned int s = _lastMeasure.indexOf(":");
//   String msg = _lastMeasure.substring(0, s) + ":" + _unit + "\0";
//   char msgB[_lenMsg];
//   Serial.print(msg);
//   msg.getBytes(msgB, _lenMsg);
//   Wire.write(msgB);
// }

void QAS_dev_sharpgp2y10::setup()
{
  QAS_Adapter::setup();
  if(_iBus == direct){
    pinMode(_ledPin, OUTPUT);
  }
  // if (_eBus == i2cebus){
  //   Wire.begin(_i2cId);
  //   Wire.onRequest(getMeasureOnRequest);
  // }
  //TODO a o outro pin non se configura?
  Serial.println("Configured Sharp gp2y10");
}

String QAS_dev_sharpgp2y10::getDirectMeasure()
{
  Serial.print("\nGet measure directly from sensor:");
  Serial.print("\nsamplingTime: ");
  Serial.println(_samplingTime);
  Serial.print("\ndeltatime: ");
  Serial.println(_deltaTime);
  float voMeasured = 0;
  float calcVoltage = 0;
  float dustDensity = 0;
  digitalWrite(_ledPin, LOW);
  delayMicroseconds(_samplingTime);

  voMeasured = analogRead(_sensorPin); // read the dust value

  delayMicroseconds(_deltaTime);
  digitalWrite(_ledPin, HIGH); // turn the LED off


  // 0 - 3.3V mapped to 0 - 1023 integer values
  // recover voltage
  calcVoltage = voMeasured * (5.0 / 1024.0);

  // linear eqaution taken from http://www.howmuchsnow.com/arduino/airquality/
  // Chris Nafis (c) 2012
  dustDensity = 0.17 * calcVoltage - 0.1;
  String ret = String(dustDensity*100) + ":" + String(dustDensity);

  Serial.print("ret: ");
  Serial.println(ret);

  if (_debugMode){
    Serial.print("Raw Signal Value (0-1023): ");
    Serial.print(voMeasured);

    Serial.print(" - Voltage: ");
    Serial.print(calcVoltage);

    Serial.print(" - Dust Density: ");// mg/m3
    Serial.println(dustDensity);
  }
  return ret;
}

String QAS_dev_sharpgp2y10::getMeasure()
{
    if (_debugMode){
        Serial.print("Reading measure from sharpgp2y10");
    }
    switch (_iBus) {
      case direct:
        return getDirectMeasure();
      case i2cibus:
        //TODO ler do bus i2c
        return "";
    }
    return "";
}

void QAS_dev_sharpgp2y10::loop()
{
  String measure = getMeasure();
  if (_debugMode == true){
    Serial.print("\nOn loop, measure: ");
    Serial.println(measure);
  }
  sendRemote(measure);
  // measure = <pm10>:<pm2.5>
}
