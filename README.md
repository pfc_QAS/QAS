# QAS An Air Quality Sensor System


## WHAT IS THIS PROJECT?

This project is an air quality sensor system.
As a sensoring system this project has both firmware and server software.
The firmware is the software that interacts directly with hardware inside the sensoring station
The server software is both the MQTT client how handle messages from sensoring stations and the web application which allow the management of the system and the public visualization of the measures that the sensoring stations sends.

The air pollution is a problem in our industrial and post-industial society. The most population in the world breathes air with pollution.
The sensoring station, in general, are very expensive and closed designed, so the population couldn't own a sensor station.
In order to democratize the air quality monitoring I've started to develop this project.

My intentions are convert this End Degree Project in a community handle project so any kind of contributions and comment are wellcome, so if you would like to help this project, please feel free to create some issue.



## WHERE IS THE CODE LOCATED

This repository has the following structure:
* *doc*: Here you can find all documentation about the project
* *experiments*: Here you can find some experiments that help me to understand how the hardware and libraries works
* *firmware*: Here you can find all the firmware of the system, both the software who are inside the microcontrollers and the main library (QAS library for Arduino Platform)
* *server*: Here you can find the server side software: the MQTT client programmed in Python and the web application build with Django framework


## HOW TO BUILD (COMPILE)

TO-DO

To build and colaborate with the project you need the following tool:
* virtual-env: https://virtualenv.pypa.io/en/stable/ You can find the requeriments.txt file in QAS->server->pfcqas->requirements.txt
* Arduino IDE: https://www.arduino.cc/en/Main/Software
* ESP-IDF: https://github.com/espressif/esp-idf


## TROUBLESHOOTING

TO-DO



## CODING STANDARDS

- Name of branches. Must be with the format of <issue_num>-<human_name_of_the_feature_or_bug>


## MAINTAINERS
- @braisarias

## CONTRIBUTORS

- @braisarias
- @filtradoraDeArrays
- @m.castrovilarino
- @eloylp